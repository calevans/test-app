import React from "react";

class Employees extends React.Component {
  // Constructor
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      DataisLoaded: false,
      url: "http://localhost:8080/employees"
    };
  }

  // ComponentDidMount is used to
  // execute the code
  componentDidMount() {
    fetch(
      this.state.url)
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          items: json,
          DataisLoaded: true
        });
      });
  }

  render() {
    const { DataisLoaded, items } = this.state;
    if (!DataisLoaded) return <div>
      <h1> Pleses wait....</h1> </div>;

    return (
      <div className="Employees">
        <h1> Employee List: </h1>
        <table>
          <thead>
          <tr>
            <th>User ID</th>
            <th>First Name</th>
            <th>First Name</th>
          </tr>
          </thead>
          <tbody>
          {
            items.map((item) => (
              <tr key={item.employeeId} >
                <td>{item.employeeId}</td>
                <td>{item.firstName}</td>
                <td>{item.lastName}</td>
              </tr>
            ))
          }
          </tbody>
        </table>
      </div>
    );
  }

}

export default Employees;
